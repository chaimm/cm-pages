import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the YomtovFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-yomtov-form',
  templateUrl: 'yomtov-form.html',
})
export class YomtovFormPage {
  yomimTovim: string[];
  name: string;
  favoriteYomTov: string;
  favoriteSong: string;
  storedYomTovReviews: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.yomimTovim = ['pesach', 'shavous', 'rosh hashana', 'yom kippur', 'sukkos',
    'chanukah', 'purim'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YomtovFormPage');
  }
  onSubmit():void {
    this.storedYomTovReviews.push({
      name: this.name,
      favoriteYomTov: this.favoriteYomTov,
      favoriteSong: this.favoriteSong,
    });

  }

}
