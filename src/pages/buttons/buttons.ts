import { Component } from '@angular/core';
import {ActionSheetController, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the ButtonsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-buttons',
  templateUrl: 'buttons.html',
})
export class ButtonsPage {
  lchaim = false;
  checker =false;
  color = 'grey' ;

  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ButtonsPage');
  }
  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Pick your color',
      buttons: [
        {
          text: 'red',
          role: 'change color to red',
          handler: () => {
            this.color = 'red';
          }
        },{
          text: 'green',
          role: 'change color to green',
          handler: () => {
            this.color = 'green';
          }
        },{
          text: 'blue',
          role: 'change color to blue',
          handler: () => {
            this.color = 'blue';
          }
        }
      ]
    });
    actionSheet.present();
  }
  toggleChecker(): void{
    this.checker = !this.checker;
  }

}
