import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import {ButtonsPage} from "../buttons/buttons";
import {ScrollablePage} from "../scrollable/scrollable";
import {ListsPage} from "../lists/lists";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  tab4Root = ButtonsPage;
  tab5Root = ListsPage;
  tab6Root = ScrollablePage;

  constructor() {

  }
}
