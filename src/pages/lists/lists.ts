import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import {ItemDetailPage} from "../item-detail/item-detail";
import {YomtovFormPage} from "../yomtov-form/yomtov-form";

/**
 * Generated class for the ListsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-lists',
  templateUrl: 'lists.html',
})
export class ListsPage {
  items:string[];


  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.items = ['menorah', 'latkes', 'dreidel', 'doughnuts'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListsPage');
  }
  goToDetail(item: string):void {
    this.navCtrl.push(ItemDetailPage, {
      item : item
    });
  }
  goToform(): void {
    this.navCtrl.push(YomtovFormPage);
  }

}


