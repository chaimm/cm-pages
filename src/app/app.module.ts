import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {ButtonsPage} from "../pages/buttons/buttons";
import {ListsPage} from "../pages/lists/lists";
import {ScrollablePage} from "../pages/scrollable/scrollable";
import {ItemDetailPage} from "../pages/item-detail/item-detail";
import {YomtovFormPage} from "../pages/yomtov-form/yomtov-form";

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ButtonsPage,
    ListsPage,
    ScrollablePage,
    ItemDetailPage,
    YomtovFormPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ButtonsPage,
    ListsPage,
    ScrollablePage,
    ItemDetailPage,
    YomtovFormPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
